export const schema = {
    fullname: {
        //Name
        step: 1,
        component: "InputField",
        label: "Name",
        fieldType: "text",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "required"
            },
            minLength: {
                params: 2,
                message: "To short!!"
            },
            maxLength: {
                params: 25,
                message: "To long!!"
            }
        },
        wrapperStyle: {
            class: "col s12 m6",
            style: {}
        },
        fieldStyle: {
            class: "indent"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    age: {
        step: 1,
        component: "InputField",
        label: "Age",
        fieldType: "number",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "required"
            }
        },
        wrapperStyle: {
            class: "col s12 m2 offset-m3"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    address: {
        step: 1,
        component: "InputField",
        label: "Address",
        fieldType: "text",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "required"
            },
            minLength: {
                params: 2,
                message: "To short!!"
            },
            maxLength: {
                params: 255,
                message: "To long!!"
            }
        },
        wrapperStyle: {
            class: "col s12 m6",
            style: {}
        },
        fieldStyle: {
            class: "indent"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    community: {
        step: 1,
        component: "InputField",
        label: "Community",
        fieldType: "text",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "Required"
            }
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    cellphone: {
        step: 1,
        component: "InputField",
        label: "Cell no.",
        fieldType: "number",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "Required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    email: {
        step: 1,
        component: "InputField",
        label: "Email",
        fieldType: "email",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "required"
            },
            email: {
                params: null,
                message: "Invalid format."
            },
        },
        wrapperStyle: {
            class: "col s12 m6",
            style: {}
        },
        fieldStyle: {
            class: "indent"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    loanNum: {
        step: 1,
        component: "InputField",
        label: "Loan #",
        fieldType: "number",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "Required"
            },
            numeric: {
                params: null,
                message: " Numbers only"
            }
        },
        wrapperStyle: {
            class: "col s12 m6"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    loanDate: {
        step: 1,
        component: "DateField",
        label: "Date of Loan",
        fieldType: "number",
        showLabel: true,
        value: "",
        validations: {
            required: {
                params: null,
                message: "Required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    jobStatus: {
        step: 2,
        component: "ConditionalSelectField",
        labels: {
            select: "What is your present status?"
        },
        fieldType: "select",
        showSelectLabel: true,
        showInputLabel: true,
        value: {},
        conditions: [{
                condition: "a",
                label: "State type of business"
            },
            {
                condition: "b",
                label: "State why"
            }
        ],
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            selectClass: "col s12 m6 ",
            inputClass: "col s12 m6 "
        },
        fieldStyle: {
            inputclass: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "Employed",
                value: "a"
            },
            {
                text: "Unemployed",
                value: "b"
            },
        ]
    },

    income: {
        step: 2,
        component: "SelectField",
        label: "If you responded to employed above indicate your range of income?",
        fieldType: "select",
        showLabel: true,
        value: "",
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "$500 - $1,000",
                value: "a"
            },
            {
                text: "$1,100 - $2,000",
                value: "b"
            },
            {
                text: "$2,000 and upwards",
                value: "c"
            },
        ]
    },
    servedeb: {
        step: 3,
        component: "SelectField",
        label: "How will you continue to service your debt?",
        fieldType: "select",
        showLabel: true,
        value: "",
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "Payments over the counter",
                value: "a"
            },
            {
                text: "Standing order",
                value: "b"
            },
            {
                text: "Payments online",
                value: "c"
            },
        ]
    },
    additionalFinance: {
        step: 3,
        component: "ConditionalSelectMultiField",
        showSelectLabel: true,
        showInputLabel: true,
        value: {},
        labels: {
            select: "Do you require additional financing?",
        },
        condition: "yes",
        wrapperStyle: {
            selectClass: "col s12 m6",
            inputClass: "col s12 m6",
        },
        fieldStyle: {},
        options: [{
                text: "Choose Option",
                value: null,
            },
            {
                text: "Yes",
                value: "yes",
            },
            {
                text: "No",
                value: "no",
            },
        ],
        validations: {
            required: {
                params: null,
                message: "Required",
            },
        },
        followUpFields: {
            whyadditional: {
                step: 2,
                component: "InputField",
                label: "If you do for what reason?",
                fieldType: "text",
                showLabel: true,
                value: "",
                validations: {
                    required: {
                        params: null,
                        message: "required"
                    },
                    minLength: {
                        params: 2,
                        message: "To short!!"
                    },
                    maxLength: {
                        params: 255,
                        message: "To long!!"
                    }
                },
                wrapperStyle: {
                    class: "col s12 m6",
                    style: {}
                },
                fieldStyle: {
                    class: "indent"
                },
                validity: {
                    showSuccess: true,
                    showError: true,
                },
            },
            Howmuch: {
                step: 3,
                component: "InputField",
                label: "How much additional financing would you require?",
                fieldType: "number",
                showLabel: true,
                value: "",
                validations: {
                    required: {
                        params: null,
                        message: "required"
                    }
                },
                wrapperStyle: {
                    class: "col s12 m6"
                },
                fieldStyle: {
                    class: "indent",
                },
                validity: {
                    showSuccess: true,
                    showError: true,
                },

            },
            securityAdditional: {
                step: 3,
                component: "SelectField",
                label: "Do you have additional security?",
                fieldType: "select",
                showLabel: true,
                value: "",
                isMultiple: false,
                validations: {
                    required: {
                        params: null,
                        message: "required"
                    },
                },
                wrapperStyle: {
                    class: "col s12 m6 "
                },
                fieldStyle: {
                    class: "indent",
                },
                validity: {
                    showSuccess: true,
                    showError: true,
                },
                options: [{
                        text: "Choose Option",
                        value: null
                    },
                    {
                        text: "Yes",
                        value: "a"
                    },
                    {
                        text: "No",
                        value: "b"
                    },

                ]
            },
            financingAdditional: {
                step: 3,
                component: "SelectField",
                label: "How would you finance the additional financing?",
                fieldType: "select",
                showLabel: true,
                value: "",
                isMultiple: false,
                validations: {
                    required: {
                        params: null,
                        message: "required"
                    },
                },
                wrapperStyle: {
                    class: "col s12 m6 "
                },
                fieldStyle: {
                    class: "indent",
                },
                validity: {
                    showSuccess: true,
                    showError: true,
                },
                options: [{
                        text: "Choose Option",
                        value: null
                    },
                    {
                        text: "Payment over the counter",
                        value: "a"
                    },
                    {
                        text: "Standing order",
                        value: "b"
                    },
                    {
                        text: "Payments online",
                        value: "c"
                    },
                ]
            },
        }
    },
    unemploymentcause: {
        step: 3,
        component: "SelectField",
        label: "Based on 1b was your unemployment due to the Covid 19 Pandemic?",
        fieldType: "select",
        showLabel: true,
        value: "",
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "Yes",
                value: "a"
            },
            {
                text: "No",
                value: "b"
            },
        ]
    },
    servicedebt: {
        step: 3,
        component: "SelectField",
        label: "Are you able to service your bebt?",
        fieldType: "select",
        showLabel: true,
        value: "",
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "Yes",
                value: "a"
            },
            {
                text: "No",
                value: "b"
            },
        ]
    },
    prefpayment: {
        step: 3,
        component: "SelectField",
        label: "Due to your present situation please tick the method of payment that would be convenient to you.",
        fieldType: "select",
        showLabel: true,
        value: "",
        isMultiple: false,
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: [{
                text: "Choose Option",
                value: null
            },
            {
                text: "Online customers - 2 months grace, payment to commence on month 3",
                value: "a"
            },
            {
                text: "3 months grace, interest paid. Full payment to commence month 4",
                value: "b"
            },
            {
                text: "Skip a loan payment for the next 5 months, full payment by month 6",
                value: "c"
            },
            {
                text: "Skip a loan payment for the next 6 months interest paid, full payment by month 7",
                value: "d"
            },

        ]
    },
    assist: {
        step: 3,
        component: "ConditionalCheckboxGroupField",
        groupTitle: "How else can the SLDB assist you? Please state type.",
        showInputLabel: true,
        value: {},
        validations: {
            required: {
                params: null,
                message: "required"
            },
        },
        wrapperStyle: {
            inputClass: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
        options: {
            training: {
                checkboxLabel: "Training",
                inputLabel: ""
            },
            technicalAssistance: {
                checkboxLabel: "Technical Assistance",
                inputLabel: ""
            },
            staffing: {
                checkboxLabel: "Staffing",
                inputLabel: ""
            },
            advertising: {
                checkboxLabel: "Advertising",
                inputLabel: ""
            },
            other: {
                checkboxLabel: "Other",
                inputLabel: ""
            },
        }
    },
}