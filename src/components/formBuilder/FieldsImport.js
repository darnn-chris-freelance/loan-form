import Vue from 'vue'

import InputField from "./fields/InputField.vue";
import SelectField from "./fields/SelectField.vue";
import DateField from "./fields/DateField.vue";
import ConditionalSelectField from "./fields/ConditionalSelectField.vue";
import ConditionalCheckboxGroupField from "./fields/ConditionalCheckboxGroupField.vue"
import ConditionalSelectMultiField from "./fields/ConditionalSelectMultiField.vue"



Vue.component('InputField', InputField)
Vue.component('SelectField', SelectField)
Vue.component('DateField', DateField)
Vue.component('ConditionalSelectField', ConditionalSelectField)
Vue.component('ConditionalCheckboxGroupField', ConditionalCheckboxGroupField)
Vue.component('ConditionalSelectMultiField', ConditionalSelectMultiField)