import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
// import './components/formBuilder/FieldsImport.js'
import vueCustomElement from 'vue-custom-element'

Vue.use(vueCustomElement);





Vue.config.productionTip = false

Vue.use(Vuelidate)

new Vue({
    render: h => h(App),
}).$mount('#app')